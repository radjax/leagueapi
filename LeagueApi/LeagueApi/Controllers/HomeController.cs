﻿
using LeagueApi.Interfaces;
using LeagueApi.Models;
using LeagueApi.Repositories;
using LeagueApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LeagueApi.Controllers
{
    public class HomeController : Controller
    {
        private ISummonerRepository summonerRepository;
        private IMatchRepository matchRepository;
        private IChampionRepository championRepository;
        private IMatchDetailRepository matchDetailRepository;
        public HomeController(ISummonerRepository summonerRepository, IMatchRepository matchRepository, IChampionRepository championRepository
            , IMatchDetailRepository matchDetailRepository)
        {
            this.summonerRepository = summonerRepository ?? throw new ArgumentNullException(nameof(summonerRepository));
            this.matchRepository = matchRepository ?? throw new ArgumentNullException(nameof(matchRepository));
            this.championRepository = championRepository ?? throw new ArgumentNullException(nameof(championRepository));
            this.matchDetailRepository = matchDetailRepository ?? throw new ArgumentNullException(nameof(matchDetailRepository));
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(string summoner)
        {
            AccountViewModel summonerAccount = await summonerRepository.GetBySummonerName(summoner);
            if(!string.IsNullOrEmpty(summonerAccount.Errors))
            {
                return RedirectToAction("RecentMatch", new RecentMatchViewModel() { Errors = summonerAccount.Errors});
            }
            MatchViewModel matches = await matchRepository.GetMatches(summonerAccount.Account.AccountId);
            if (!string.IsNullOrEmpty(matches.Errors))
            {
                return RedirectToAction("RecentMatch", new RecentMatchViewModel() { Errors = matches.Errors });
            }
            IMatch match = matches.matches.FirstOrDefault();
            
            MatchDetailsViewModel matchDetails = await matchDetailRepository.GetMatchDetails(match.GameId.ToString());
            if (!string.IsNullOrEmpty(matchDetails.Errors))
            {
                return RedirectToAction("RecentMatch", new RecentMatchViewModel() { Errors = matchDetails.Errors });
            }
            IEnumerable<IChampion> champions = await championRepository.GetChampionList();
            IMatchParticipant matchParticpant = matchDetails.matchDetails.Participants.Where(x => x.ChampionID == match.Champion).FirstOrDefault();

            RecentMatchViewModel recentMatchViewModel = new RecentMatchViewModel()
            {
                SummonerName = summonerAccount.Account.Name,
                Errors =  "",
                Champion = champions.Where(x => x.Id == match.Champion).FirstOrDefault().Name,
                Kills = matchParticpant.Stats.Kills,
                Deaths = matchParticpant.Stats.Deaths,
                Assists = matchParticpant.Stats.Assists,
                TotalDamageDealt = matchParticpant.Stats.TotalDamageDealt
            };

            return RedirectToAction("RecentMatch", recentMatchViewModel);
        }

        public ActionResult RecentMatch(RecentMatchViewModel viewModel)
        {
            return View(viewModel);
        }
        
    }
}