// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LeagueApi.DependencyResolution {
    using LeagueApi.Interfaces;
    using LeagueApi.Models;
    using LeagueApi.Repositories;
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
	
    public class DefaultRegistry : Registry {
        #region Constructors and Destructors

        public DefaultRegistry() {
            Scan(
                scan => {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
					scan.With(new ControllerConvention());
                });
            //For<IExample>().Use<Example>();
            For<ISummonerRepository>().Use<SummonerRepository>();
            For<IMatchRepository>().Use<MatchRepository>();
            For<IChampionRepository>().Use<ChampionRepository>();
            For<IMatchDetailRepository>().Use<MatchDetailRepository>();
            For<ISummoner>().Use<Summoner>();
            For<IMatch>().Use<Match>();
            For<IMatchList>().Use<MatchList>();
            For<IChampion>().Use<Champion>();
            For<IMatchDetails>().Use<MatchDetails>();
            For<IMatchParticipant>().Use<MatchParticipant>();
            For<IParticipantStats>().Use<ParticipantStats>();
        }

        #endregion
    }
}