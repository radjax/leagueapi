﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Interfaces
{
    public interface IChampion
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}