﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Interfaces
{
    public interface IMatch
    {
        string Lane { get; set; }
        Int64 GameId { get; set; }
        int Champion { get; set; }
        Int64 TimeStamp { get; set; }
        int Queue { get; set; }
        string Role { get; set; }
        int Season { get; set; }
    }
}