﻿using LeagueApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LeagueApi.Interfaces
{
    public interface IMatchDetailRepository
    {
        Task<MatchDetailsViewModel> GetMatchDetails(string matchId);
    }
}