﻿using LeagueApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Interfaces
{
    public interface IMatchList
    {
        IEnumerable<Match> matches { get; set; }
    }
}