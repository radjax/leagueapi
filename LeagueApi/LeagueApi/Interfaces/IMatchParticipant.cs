﻿using LeagueApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Interfaces
{
    public interface IMatchParticipant
    {
        int ChampionID { get; set; }
        string HighestAchievedSeasonTier { get; set; }
        int ParticipantId { get; set; }
        ParticipantStats Stats { get; set; }
    }
}