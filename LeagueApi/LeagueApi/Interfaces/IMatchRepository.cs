﻿using LeagueApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LeagueApi.Interfaces
{
    public interface IMatchRepository
    {
        Task<MatchViewModel> GetMatches(int accountID);
    }
}