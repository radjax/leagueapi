﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Interfaces
{
    public interface IParticipantStats
    {
        int Kills { get; set; }
        int Deaths { get; set; }
        int Assists { get; set; }
        int TotalDamageDealt { get; set; }
    }
}