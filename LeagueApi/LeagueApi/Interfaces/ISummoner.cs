﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Interfaces
{
    public interface ISummoner
    {
        int AccountId { get; set; }
        int ProfileIconId { get; set; }
        string Name { get; set; }
        int SummonerLevel { get; set; }
    }
}