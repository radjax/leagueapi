﻿using LeagueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Models
{
    public class Champion: IChampion
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}