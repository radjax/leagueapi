﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Models
{
    public class JsonResult
    {
        public Status Status { get; set; }
    }

    public class Status
    {
        public string Message { get; set; }
        public string StatusCode { get; set; }
    }
}