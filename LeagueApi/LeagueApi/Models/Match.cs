﻿using LeagueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Models
{
    public class Match : IMatch
    {
        public string Lane { get; set; }
        public Int64 GameId { get; set; }
        public int Champion { get; set; }
        public Int64 TimeStamp { get; set; }
        public int Queue { get; set; }
        public string Role { get; set; }
        public int Season { get; set; }
    }
}