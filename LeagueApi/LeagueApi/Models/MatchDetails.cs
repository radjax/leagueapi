﻿using LeagueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Models
{
    public class MatchDetails : IMatchDetails
    {
        public List<MatchParticipant> Participants { get; set; }
    }
}