﻿using LeagueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Models
{
    public class MatchList : IMatchList
    {
        public IEnumerable<Match> matches { get; set; }
    }
}