﻿using LeagueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Models
{
    public class MatchParticipant : IMatchParticipant
    {
        public int ChampionID { get; set; }
        public string HighestAchievedSeasonTier { get; set; }
        public int ParticipantId { get; set; }
        public ParticipantStats Stats { get; set; }
    }
}