﻿using LeagueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Models
{
    public class ParticipantStats : IParticipantStats
    {
        public int Kills { get; set; }
        public int Deaths { get; set; }
        public int Assists { get; set; }
        public int TotalDamageDealt { get; set; }
    }
}