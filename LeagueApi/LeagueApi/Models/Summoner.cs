﻿using LeagueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.Models
{
    public class Summoner : ISummoner
    {
        public int AccountId { get; set; }
        public int ProfileIconId { get; set; }
        public string Name { get; set; }
        public int SummonerLevel { get; set; }
    }
}