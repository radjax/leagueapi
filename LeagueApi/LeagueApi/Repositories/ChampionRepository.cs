﻿using LeagueApi.Interfaces;
using LeagueApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace LeagueApi.Repositories
{
    public class ChampionRepository : IChampionRepository
    {
        private static readonly HttpClient client = new HttpClient();
        public async Task<IEnumerable<IChampion>> GetChampionList()
        {
            var response1 = await client.GetAsync("https://api.jsonbin.io/b/5b55303eefaed72daef62ec7");
            var responseString1 = response1.Content.ReadAsStringAsync();
            IEnumerable<IChampion> champions = JsonConvert.DeserializeObject<IEnumerable<Champion>>(responseString1.Result);
            return champions;
        }
    }
}