﻿using LeagueApi.Interfaces;
using LeagueApi.Models;
using LeagueApi.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace LeagueApi.Repositories
{
    public class MatchDetailRepository : IMatchDetailRepository
    {
        private static readonly HttpClient client = new HttpClient();

        public async Task<MatchDetailsViewModel> GetMatchDetails(string matchId)
        {
            MatchDetailsViewModel viewModel = new MatchDetailsViewModel();
            string apiKey = System.Configuration.ConfigurationManager.AppSettings["ApiKey"];

            var response = await client.GetAsync("https://na1.api.riotgames.com/lol/match/v3/matches/" + matchId + "?api_key=" + apiKey);
            var responseString = response.Content.ReadAsStringAsync();
            IMatchDetails matchDetails = JsonConvert.DeserializeObject<MatchDetails>(responseString.Result);
            if (matchDetails == null)
            {
                JsonResult result = JsonConvert.DeserializeObject<JsonResult>(responseString.Result);
                if (result.Status.Message == "Forbidden")
                {
                    viewModel.Errors = "Api Key Expired";
                }
                else if (result.Status.Message == "Data not found")
                {
                    viewModel.Errors = "Could Not Find Match Details";
                }
                else
                {
                    viewModel.Errors = "Error Reading From API";
                }
            }
            else
            {
                viewModel.matchDetails = matchDetails;
            }

            return viewModel;
        }
    }
}