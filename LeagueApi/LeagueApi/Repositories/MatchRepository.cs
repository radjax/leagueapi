﻿using LeagueApi.Interfaces;
using LeagueApi.Models;
using LeagueApi.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace LeagueApi.Repositories
{
    public class MatchRepository : IMatchRepository
    {
        private static readonly HttpClient client = new HttpClient();

        public async Task<MatchViewModel> GetMatches(int accountID)
        {
            MatchViewModel viewModel = new MatchViewModel();
            string apiKey = System.Configuration.ConfigurationManager.AppSettings["ApiKey"];

            var response = await client.GetAsync("https://na1.api.riotgames.com/lol/match/v3/matchlists/by-account/" + accountID + "?api_key=" + apiKey);
            var responseString = response.Content.ReadAsStringAsync();
            IMatchList matches = JsonConvert.DeserializeObject<MatchList>(responseString.Result);
            if (matches.matches == null)
            {
                JsonResult result = JsonConvert.DeserializeObject<JsonResult>(responseString.Result);
                if (result.Status.Message == "Forbidden")
                {
                    viewModel.Errors = "Api Key Expired";
                }
                else if (result.Status.Message == "Data not found")
                {
                    viewModel.Errors = "Could Not Find Matches";
                }
                else
                {
                    viewModel.Errors = "Error Reading From API";
                }
            } else
            {
                viewModel.matches = matches.matches.ToList();
            }

            return viewModel;
        }
    }
}