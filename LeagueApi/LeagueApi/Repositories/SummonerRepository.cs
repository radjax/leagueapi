﻿using LeagueApi.Interfaces;
using LeagueApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using LeagueApi.ViewModels;

namespace LeagueApi.Repositories
{
    public class SummonerRepository : ISummonerRepository
    {
        private static readonly HttpClient client = new HttpClient();

        public async Task<AccountViewModel> GetBySummonerName(string summoner)
        {
            AccountViewModel viewModel = new AccountViewModel();
            string apiKey = System.Configuration.ConfigurationManager.AppSettings["ApiKey"];
            string uri = "https://na1.api.riotgames.com/lol/summoner/v3/summoners/by-name/" + summoner + "?api_key=" + apiKey;
            var response = await client.GetAsync(uri);
            var responseString = response.Content.ReadAsStringAsync();
            ISummoner account = JsonConvert.DeserializeObject<Summoner>(responseString.Result);
            if (account == null || account.Name == null)
            {
                JsonResult result = JsonConvert.DeserializeObject<JsonResult>(responseString.Result);
                if (result.Status.Message == "Forbidden")
                {
                    viewModel.Errors = "Api Key Expired";
                }
                else if (result.Status.Message == "Data not found")
                {
                    viewModel.Errors = "Could Not Find Account";
                }
                else
                {
                    viewModel.Errors = "Error Reading From API";
                }
            }
            else
            {
                viewModel.Account = account;
            }
            return viewModel;
        }
        
    }
}