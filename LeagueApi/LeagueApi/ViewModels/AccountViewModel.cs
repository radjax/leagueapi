﻿using LeagueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.ViewModels
{
    public class AccountViewModel
    {
        public ISummoner Account { get; set; }
        public string Errors { get; set; }
    }
}