﻿using LeagueApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.ViewModels
{
    public class MatchDetailsViewModel
    {
        public IMatchDetails matchDetails { get; set; }
        public string Errors { get; set; }
    }
}