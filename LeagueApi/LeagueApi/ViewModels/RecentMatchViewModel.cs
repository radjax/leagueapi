﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeagueApi.ViewModels
{
    public class RecentMatchViewModel
    {
        public string SummonerName { get; set; }
        public string Champion { get; set; }
        public int Kills { get; set; }
        public int Deaths { get; set; }
        public int Assists { get; set; }
        public int TotalDamageDealt { get; set; }
        public string Errors { get; set; }
    }
}